# ac-computadora-ii

```plantuml
@startmindmap
title Mapa conceptual - Computadoras electrónicas   
*[#Red] Computadoras\nelectrónicas 
**[#Orange] Los Relés se reemplazan por tubos de vacio
***[#lightgreen] La primera computadora en usar tubos de vacio\nfue la Colossus Mark 1
****[#Blue] Contaba con 1600 tubos de vacio y se fabricaron 10 Colossus
*****[#Yellow] Ayudaba a la decodificación de mensajes NAZI
***[#lightgreen] Después llega la ENIAC (1946, Universidad de Pensilvanía)
****[#Blue] Podía hacer 5000 operaciones aritméticas por segundo

***[#lightgreen] En 1947, se crea el transistor
****[#Blue] La primera computadora hecha basada en transistores podía hacer 4500 sumas\n 80 divisiones o multiplicaciones por segundo
*****[#Yellow] IBM usó transistores en sus productos para acercar las computadoras a oficinas\ny hogares
left side
**[#Orange] Nacen por los inconvenientes que presentaban\nlas computadoras electromecánicas
***[#lightgreen] Costos altos de mantenimiento
***[#lightgreen] Relés
****[#Blue] No eran muy confiables 
****[#Blue] Atraían bichos
@endmindmap
```

```plantuml
@startmindmap
title Mapa conceptual -  Arquitectura Von Neumann y Harvard
*[#Red] Arquitectura Von Neumann y Harvard
**[#Orange] Antes habia sistemas cableados
***[#lightgreen] Datos -> Secuencia de funciones aritméticas/logicas -> Resultados
***[#lightgreen] Ahora se añade Interprete de instrucciones y señales de control a Datos -> Secuencia de funciones aritméticas/logicas -> Resultados

****[#green] Arquitectura Von Neumann
*****[#lightgreen] Modelo
******[#lightgreen] Los datos y programa  se almacenas en una misma memoria
*******[#lightgreen] Los contenidos de esta memoria se acceden indicando su posición sin importar su tipo
********[#lightgreen] Se ejecutan en secuencia a menos que se indique lo contrario
*********[#lightgreen] Su representación es binaría 
*****[#lightgreen] Contiene 
******[#lightgreen] CPU
*******[#lightgreen] Es el nucleo central del computador
********[#lightgreen] Realiza operaciones básicas
********[#lightgreen] Gestiona funcionamiento del resto de componentes
******[#lightgreen] Memoria principal
*******[#lightgreen] se almacenan
********[#lightgreen] Datos
********[#lightgreen] Instrucciones
******[#lightgreen] Buses
*******[#lightgreen] Permite la comunicación entre los distintos bloques del sistema
******[#lightgreen] Periféricos
*******[#lightgreen] Se encargan de 
********[#lightgreen] Tomar datos / mostrar salida
********[#lightgreen] Comunicarse con otros sistemas
*****[#lightgreen] La separación de la memoria y CPU acarreó un problema denominado cuella de botella de Neumann
******[#lightgreen] Se debe a que a la diferencia de velocidad entre los 2 elementos, la CPU puede permanecer ociosa
*****[#lightgreen] Describe una arquitectura de diseño para una PC digital

****[#Red] Arquitectura Harvard
*****[#Blue] Modelo
******[#Blue] Se referia a la arquitectura que utiliza un dispositivo de almacenamiento fisicamente separados para instrucciones y datos
*****[#Blue] Inconvenientes
******[#Blue] Se dividia la cache solo cuando la frecuencia de lectura de instrucciones y datos era aprox. la misma
*****[#Blue] Solución
******[#Blue] Las instrucciones y datos se alamcenaban en caches separadas para mejorar el rendimiento.
*****[#Blue] Contiene
******[#Blue] Unidad central de procesamiento
*******[#Blue] Contiene una unidad de control, unidad aritmética / lógica y registros
******[#Blue] Memoria principal
*******[#Blue] Almacena instrucciones y datos
******[#Blue] Sistemas E\S
*****[#Blue] Fabricar memorias más rapidas tiene un precio alto, la solución fue proporcionar una memoria, conocida como CACHE

**[#Orange] Ley de Moore
***[#lightgreen] establece que la velocidad del procesador se duplica cada 2 meses 
****[#Blue] El número de transistores por chip se ducplica cada año
*****[#Red] El costo del chip permanece sin cambios
******[#Gray] Cada 18 meses se duplica la potenia de calculo sin modificar el costo
@endmindmap
```

```plantuml
@startmindmap
title Mapa conceptual -  Basura Electrónica(RAEE)
*[#Crimson] Basura Electrónica
**[#Aquamarine] Se tiran cosas para comprar lo nuevo
**[#Aquamarine] Principalmente son TV's, PC's, celulares
***[#Chocolate] Los que botan estos aparatos piensan que ya no valen nada
****[#Lightseagreen] Pero de estos se extrae Oro, Plata, Bronce, Cobalto, varios plasticos, etc.
*****[#Lime] Si no se tratan de la manera adecuada puede ser muy peligroso
******[#Violet] Para el ser humano como para el medio ambiente.
**[#Aquamarine] ¿Cómo se trata?
***[#Chocolate] Primero se recolectan
****[#Lightseagreen] En ocasiones se pesa para calcular el pago por el producto reciclado
*****[#Lime] Después se clasifican por categoría
******[#Violet] Se desarmen
*******[#RoyalBlue] Se extraen los materiales
********[#Red] Se usan para elaborar nuevos productos
*********[#Indigo] En promedio se aprovecha un 95% del aparato
*********[#Indigo] Dependiendo del material se busca cómo reutilizarlo\n(Ej. Plastico se puede utilizar para elaborar suelas de zapatos)
**[#Aquamarine] En el caso de los discos duros
***[#Chocolate] Normalmente primero se les borra la información
****[#Lightseagreen] Para después destruirse, por temas de privacidad
*****[#Lime] Generalmente las empresas lo exigen.
**[#Aquamarine] No solo particulares tiran aparatos que segun ellos ya no sirven
***[#Chocolate] Las empresas desechan muchos aparatos que para ellos ya son obsoletos
****[#Lightseagreen] En ocasiones se pueden reparar si vienen con alguna falla
**[#Aquamarine] Para algunas personas se encuentrar tesoros
***[#Chocolate] Refiriendose a productos retro
****[#Lightseagreen] Algunos todavía pueden ser reacondicionados
****[#Lightseagreen] Se ve como una vista o viaje al pasado
*****[#Lime] Crea una sensación de nostalgia
****[#Lightseagreen] Algunos todavía pueden ser reacondicionados 
@endmindmap
```

